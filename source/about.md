---
title: About FFS
date: 2021-01-21 16:22:02
---

参见[meta :: ffs speak](/2021/03/ffs-speak/)

Masayuki "Jojo" Takayanagi & New Direction For The Arts ‎– [Free Form Suite](https://www.youtube.com/watch?v=sdMCqNPv1AA)

* 2019.5.21 *Free Form Suite*发布了第一条博文。
* 从2021.1.21开始，*Free Form Suite*从[wordpress](https://freeformsuite.wordpress.com/)暂时转移至[github](https://github.com/xissshawww/xissshawww.github.io)。

推荐使用Reeder或者NetNewsWire[订阅本站rss](http://freeformsuite.guru/atom.xml)

你可以在[这里](https://github.com/xissshawww/ffs_posts/archive/master.zip)下载所有的posts.