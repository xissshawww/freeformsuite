---
title: 'Varèse :: The Liberation of Sound'
date: 2021-02-03 10:01:12
tags:
---

> Our musical alphabet is poor and illogical.
> Music, which should pulsate with life, needs new means of expression, and science alone can infuse it with youthful vigor.
> Why Italian Futurists, have you slavishly reproduced only what is commonplace and boring in the bustle of our daily lives.
> I dream of instruments obedient to my thought and which with their contribution of a whole new world of unsuspected sounds, will lend themselves to the exigencies of my inner rhythm.[^1]
> (Excerpts from lectures by Varese, compiled and edited with footnotes by Chou Wen-chung)[^2]

## New Instruments and New Music[^3]

When new instruments will allow me to write music as I conceive it, taking the place of the linear counterpoint, the movement of sound-masses, of shifting planes, will be clearly perceived. When these sound-masses collide the phenomena of penetration or repulsion will seem to occur. Certain transmutations taking place on certain planes will seem to be projected onto other planes, moving at different speeds and at different angles. There will no longer be the old conception of melody or interplay of melodies. The entire work will be a melodic totality. The entire work will flow as a river flows.

Today with the technical means that exist and are easily adaptable, the differentiation of the various masses and different planes as these beams of sound, could be made discernible to the listener by means of certain acoustical arrangements. Moreover, such an acoustical arrangement would permit the delimitation of what I call Zones of Intensities. These zones would be differentiated by various timbres or colors and different loudnesses. Through such a physical process these zones would appear of different colors and of different magnitude in different perspectives for our perception. The role of color or timbre would be completely changed from being incidental, anecdotal, sensual or picturesque, it would become an agent of delineation like the different colors on a map separating different areas, and an integral part of form. These zones would be felt as isolated, and the hitherto unobtainable non-blending (or at least the sensation of non-blending) would become possible.

In the moving masses you would be conscious of their transmutations when they pass over different layers, when they penetrate certain opacities, or are dilated in certain rarefactions. Moreover, the new musical apparatus I envisage, able to emit sounds of any number of frequencies, will extend the limits of the lowest and highest registers, hence new organizations of the vertical resultants: chords, their arrangements, their spacings, that is, their oxygenation. Not only will the harmonic possibilities of the overtones be revealed in all their splendor but the use of certain interferences created by the partials will represent an appreciable contribution. The never before thought of use of the inferior resultants and of the differential and additional sounds may also be expected. An entirely new magic of sound!

I am sure that the time will come when the composer, after he has graphically realized his score, will see this score automatically put on a machine which will faithfully transmit the musical content to the listener. As frequencies and new rhythms will have to be indicated on the score, our actual notation will be inadequate. The new notation will probably be seismographic. And here it is curious to note that at the beginning of two eras, the Mediaeval primitive and our own primitive era (for we are at a new primitive stage in music today) we are faced with an identical problem: the problem of finding graphic symbols for the transposition of the composer's thought into sound. At a distance of more than a thousand years we have this analogy: our still primitive electrical instruments find it necessary to abandon staff notation and to use a kind of seismographic writing much like the early ideographic writing originally used for the voice before the development of staff notation. Formerly the curves of the musical line indicated the melodic fluctuations of the voice, today the machine-instrument requires precise design indications.

## Music as an Art-science[^4]

And here are the advantages I anticipate from such a machine: liberation from the arbitrary, paralyzing tempered system; the possibility of obtaining any number of cycles or if still desired, subdivisions of the octave, consequently the formation of any desired scale; unsuspected range in low and high registers; new harmonic splendors obtainable from the use of sub-harmonic combinations now impossible; the possibility of obtaining any differentiation of timbre, of sound-combinations; new dynamics far beyond the present human-powered orchestra; a sense of sound-projection in space by means of the emission of sound in any part or in many parts of the hall as may be required by the score; cross rhythms unrelated to each other, treated simultaneously, or to use the old word, "contrapuntally" (since the machine would be able to beat any number of desired notes, any subdivision of them, omission or fraction of them) - all these in a given unit of measure or time which is humanly impossible to attain.

## Rhythm, Form and Content[^5]

My fight for the liberation of sound and for my right to make music with any sound and all sounds has sometimes been construed as a desire to disparage and even to discard the great music of the past. But that is where my roots are. No matter how original, how different a composer may seem, he has only grafted a little bit of himself on the old plant. But this he should be allowed to do without being accused of wanting to kill the plant. He only wants to produce a new flower. It does not matter if at first it seems to some people more like a cactus than a rose. Many of the old masters are my intimate friends - all are respected colleagues. None of them are dead saints - in fact none of them are dead - and the rules they made for themselves are not sacrosanct and are not everlasting laws. Listening to music by Perotin, Machaut, Monteverdi, Bach, or Beethoven we are conscious of living substances; they are "alive in the present." But music written in the manner of another century is the result of culture and, desirable and comfortable as culture my be, an artist should not lie down in it. The best bit of criticism André Gide ever wrote was this confession, which must have been wrung from him by self-torture: 

> "When I read Rimbaud or the Sixth Song of Maldorer, I am ashamed of my own works and everything that is only the result of culture."

Because for so many years I crusaded for new instruments with what may have seemed fanatical zeal, I have been accused of desiring nothing less than the destruction of all musical instruments and even of all performers. This is, to say the least, an exaggeration. Our new liberating medium - the electronic - is not meant to replace the old musical instruments which composers, including myself, will continue to use. Electronics is an additive, not a destructive factor in the art and science of music. It is because new instruments have been constantly added to the old ones that Western music has such a rich and varied patrimony.

Grateful as we must be for the new medium, we should not expect miracles from machines. The machine can give out only what we put into it. The musical principles remain the same whether a composer writes for orchestra or tape. Rhythm and Form are still his most important problems and the two elements in much most generally misunderstood.

Rhythm is too often confused with metrics. Cadence or the regular succession of beats and accents has little to do with the rhythm of a composition. Rhythm is the element in music that gives life to the work and holds it together. It is the element of stability, the generator of form. In my own works, for instance, rhythm derives from the simultaneous interplay of unrelated elements that intervene at calculated, but not regular time lapses. This corresponds more nearly to the definition of rhythm in physics and philosophy as "a succession of alternate and opposite or correlative states."

As for form, Busoni once wrote: "is it not singular to demand of a composer originality in all things and to forbid it as regards form? No wonder that if he is original he is accused of formlessness."[^6]

The misunderstanding has come from thinking of form as a point of departure, a pattern to be followed, a mold to be filled. Form is a result - the result of a process. Each of my works discovers its own form, I could never have fitted them into any of the historical containers. If you want to fill a rigid box of a definite shape, you must have something to put into it that is the same shape and size or that is elastic or soft enough to be made to fit in. But if you try to force into it something of a different shape and harder substance, even if its volume and size are the same, it will break the box. My music cannot be made to fit into any of the traditional music boxes.

Conceiving musical form as a resultant - the result of a process, I was struck what seems to me an analogy between the formation of my compositions and the phenomenon of crystallization. Let me quote the crystallographic description given me by Nathaniel Arbiter, professor of mineralogy at Columbia University:

> "The crystal is characterized by both a definite external forma in a definite internal structure. The internal structure is based on the unit of crystal which is the smallest grouping of the atoms that has the order and composition of the substance. The extension of the unit into space forms the whole crystal. But in spite of the relatively limited variety of internal structures, the external forms of crystals are limitless."

Then Mr. Arbiter added in his own words: "Crystal form itself is a resultant (the very word I have always used in reference to musical form) rather than a primary attribute. Crystal form is the consequence of the interaction of attractive and repulsive forces and the ordered packing of the atom."

This, I believe, suggests better than any explanation I could give about the way my works are formed. There is an idea, the basis of an internal structure, expanded and split into different shapes or groups of sound constantly changing in shape, directions, and speed, attracted and repulsed by various forces. The form of the work is the consequence of this interaction. Possible musical forms are as limitless as the exterior forms of crystals.

Connected with this contentious subject of form in music is the really futile question of the difference between form and content. There is no difference. Form and content are one. Take away form, and there is no content, and if there is no content there is only a rearrangement of musical patterns, but no form. Some people go so far as to suppose that the content of what is called program music is the subject described. This subject is only the ostensible motive I have spoken of, which in program music the composer chooses to reveal. The content is still only music. The same senseless bickering goes on over style and content in poetry. We could very well transfer to the question of music what Samuel Beckett has said of Proust: "For Proust the quality of language is more important than any system of ethics or esthetics. Indeed he makes no attempt to dissociate form from content. The one is the concretion of the other - the revelation of a world."[^7] To reveal a new world is the function of creation in all the arts, but the act of creation defies analysis. A composer knows about as little as anyone else about where the substance of his work comes from.

As an epigraph to his book[^8], Busoni uses this verse from a poem by the Danish poet, Oelenschläger:

> "What seek you? Say! And what do you expect?
> I know not what; the Unknown I would have!
> What's known to me is endless; I would go
> Beyond the known: The last word still is wanting."
>
> (Der mdchtige Zauberer)

And so it is for any artist.

## The Electronic Medium[^9]

First of all I should like you to consider what I believe is the best definition of music, because it is all-inclusive: "the corporealization of the intelligence that is in sound," as proposed by Hoëne Wronsky[^10]. If you think about it you will realize that, unlike most dictionary definitions which make use of such subjective terms as beauty, feeling, etc., it covers all music, Eastern or Western, past or present, including the music of our new electronic medium. Although this new music is being gradually accepted, there are still people who, while admitting that it is "interesting," say, "but is it music?" It is a question I am only too familiar with. Until quite recently I used hear is so often in regard to my own works, that, as far back as the twenties, I decided to my music "organized sound" and myself, not a musician, but "a worker in rhythms, frequencies, and intensities." Indeed, to stubbornly conditioned ears, anything new in music has always been called noise. But after all what is music but organized noises? And a composer, like all artists, is an organizer of disparate elements. Subjectively, noise is any sound one doesn't like.

Our new medium has brought to composers almost endless possibilities of expression, and opened up for them the whole mysterious world of sound. For instance, I have always felt the need of a kind of continuous flowing curve that instruments could not give me. That is why I used sirens in several of my works. Today such effects are easily obtainable by electronic means. In this connection it is curious to note that it is this lack of flow that seems to disturb Eastern musicians in our Western music. To their ears it does not glide, sounds jerky, composed of edges of intervals and holes and, as an Indian pupil of mine expressed it, "jumping like a bird from branch to branch." To them apparently our Western music seems to sound much as it sounds to us when a record is played backward. But playing a Hindu record of a melodic vocalization backward, I found that it had the same smooth flow as when played normally, scarcely altered at all.

The electronic medium is also adding an unbelievable variety of new timbres to our musical store, but most important of all, it has freed music from the tempered system, which has prevented music from keeping pace with the other arts and with science. Composers are now able, as never before, to satisfy the dictates of that inner ear of the imagination. They are also lucky so far in not being hampered by esthetic codification - at least not yet! But I am afraid it will not be long before some musical mortician begins embalming electronic music in rules.

We should also remember that no machine is a wizard, as we are beginning to think, and we must not expect our electronic devices to compose for us. Good music and bad music will be composed by electronic means, just as good and bad music have been composed for instruments. The computing machine is a marvelous invention and seems almost superhuman. But, in reality, it is as limited as the mind of the individual who feeds it material. Like the computer, the machines we use for making music can only give back what we put into them. But, considering the fact that our electronic devices were never meant for making music, but for the sole purpose of measuring and analyzing sound, it is remarkable that what has already been achieved as musically valid. They are still somewhat unwieldy and time-consuming and not entirely satisfactory as an art-medium. But this new art is still in its infancy, and I hope and firmly believe, now that composers and physicists are at least working together, and music is again linked with science, as it was in the Middle Ages, that new and more musically efficient devices will be invented.

——Edited and Annotated by Chou Wen-chung

Edgard Varèse, 1936

[^1]: "391," Number 5, June 1917, New York; translated from the French by Louise Varese.
[^2]: The complete Varese lectures will appear in the forthcoming book Contemporary Composers on Contemporary Music, to be published by Holt, Rinehart, and Winston.
[^3]: From a lecture given at Mary Austin House, Santa Fe, 1936.
[^4]: From a lecture given at the University of Southern California, 1939.
[^5]: From a lecture given at Princeton University, 1959.
[^6]:  See note 8.
[^7]: Samuel Beckett, Proust (1957).
[^8]: Ferruccio Busoni (1866-1924), Entwurf einer neuen Asthetik der Tonkunst (1907); published in English as Sketch of a New Esthetic of Music (T. Baker, tr.; 1911).
[^9]: From a lecture given at Yale University, 1962.H
[^10]: oine Wronsky (1778-1853), also known as Joseph Marie Wronsky, was a Polish philosopher and mathematician, known for his system of Messianism. Camille Durutte (1803-81), in his Technie Harmonique (1876), a treatise on "musical mathe- matics," quoted extensively from the writings of Wronsky.