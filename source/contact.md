---
title: Contact
date: 2019-05-21 03:34:54
---

tg[@nonxishaw](https://t.me/nonxishaw)
mastodon: [ni.hil.ist/@xissshawww](ni.hil.ist/@xissshawww)
twitter: [@xissshawww](https://twitter.com/xissshawww)
PGP: [~xissshawww.pgp](https://meta.sr.ht/~xissshawww.pgp)
