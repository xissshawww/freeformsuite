---
title: Lib
date: 2021-04-02
---

## ø
* [巴塔耶-圣神·死人](https://gitlab.com/xissshawww/ffs-lib/-/raw/31998887e82643a9ac74fb29715a468b808da874/georges-bataille-divinus-deus-suivi-de-le-mort.pdf?inline=false)
* [manifestos](https://gitlab.com/xissshawww/ffs-lib/-/raw/31998887e82643a9ac74fb29715a468b808da874/manifestos.pdf?inline=false)

## Semiotext(e)
* [Semiotexte_Vol_3_No_1_Nietzsches_Return](https://gitlab.com/xissshawww/ffs-lib/-/raw/31998887e82643a9ac74fb29715a468b808da874/Semiotexte_Vol_3_No_1_Nietzsches_Return.pdf?inline=false)
* [Semiotexte_Vol_3_No_2_Schizo-Culture](https://gitlab.com/xissshawww/ffs-lib/-/raw/31998887e82643a9ac74fb29715a468b808da874/Semiotexte_Vol_3_No_2_Schizo-Culture.pdf?inline=false)
* [Semiotexte_2_3_Anti-Oedipus_From_Psychoanalysis_to_Schizopolitics_1977](https://gitlab.com/xissshawww/ffs-lib/-/raw/31998887e82643a9ac74fb29715a468b808da874/Semiotexte_2_3_Anti-Oedipus_From_Psychoanalysis_to_Schizopolitics_1977.pdf?inline=false)

## Tiqqun
* [Critical Metaphysics](https://gitlab.com/xissshawww/ffs-lib/-/raw/31998887e82643a9ac74fb29715a468b808da874/Tiqqun-Critical-Metaphysics-READ.pdf?inline=false)
* [Intro to Civil War / IWE](https://gitlab.com/xissshawww/ffs-lib/-/raw/31998887e82643a9ac74fb29715a468b808da874/Tiqqun-Intro-to-Civil-War-IWE-READ.pdf?inline=false)

## Éclats
* [Éclats-Call](https://gitlab.com/xissshawww/ffs-lib/-/raw/31998887e82643a9ac74fb29715a468b808da874/eclats-call.pdf?inline=false)

## Numogram
* [Unleashing-the-Numogram](https://gitlab.com/xissshawww/ffs-lib/-/raw/31998887e82643a9ac74fb29715a468b808da874/nleashing-the-Numogram.pdf?inline=false)
* [Understanding-Nick-Land's-Time-Sorcery-System-tables](https://gitlab.com/xissshawww/ffs-lib/-/raw/31998887e82643a9ac74fb29715a468b808da874/Understanding-Nick-Land's-Time-Sorcery-System-tables.xlsx?inline=false)